package com.devcamp.customerorderapi.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerorderapi.model.CCustomer;
import com.devcamp.customerorderapi.model.COrder;
import com.devcamp.customerorderapi.repository.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository pCustomerRepository;
    public ArrayList<CCustomer> getAllCustomers() {
        ArrayList<CCustomer> listCustomer = new ArrayList<>();
        pCustomerRepository.findAll().forEach(listCustomer::add);
        return listCustomer;
    }
    
    public Set<COrder> getOrderByCustomerID(long id) { 
        CCustomer vCustomer = pCustomerRepository.findByCustomerId(id);
        if (vCustomer != null) {
            return  vCustomer.getOrders();
        } else {
            return null;
        }
    }
}
