package com.devcamp.customerorderapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorderapi.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long>{
    CCustomer findByCustomerId(long customerId);
    
}
