package com.devcamp.customerorderapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorderapi.model.COrder;

public interface IOrderReposity extends JpaRepository<COrder,Long>{
    
}
