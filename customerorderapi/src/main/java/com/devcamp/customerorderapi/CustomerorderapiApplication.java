package com.devcamp.customerorderapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerorderapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerorderapiApplication.class, args);
	}

}
